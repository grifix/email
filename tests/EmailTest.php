<?php

declare(strict_types=1);

namespace Grifix\Email\Tests;

use Grifix\Email\Email;
use Grifix\Email\InvalidEmailException;
use PHPUnit\Framework\TestCase;

final class EmailTest extends TestCase
{
    public function testItCreates(): void
    {
        self::assertEquals('user@example.com', Email::create('user@example.com')->toString());
        self::assertEquals('user@example.com', (string)Email::create('user@example.com'));
    }

    public function testIsEqualTo(): void
    {
        self::assertTrue(
            Email::create('user@example.com')
                ->isEqualTo(Email::create('user@example.com'))
        );

        self::assertFalse(
            Email::create('user@example.com')
                ->isEqualTo(Email::create('user2@example.com'))
        );
    }

    /**
     * @dataProvider invalidEmailDataProvider
     */
    public function testItDoesNotCreate(string $value): void
    {
        $this->expectException(InvalidEmailException::class);
        $this->expectExceptionMessage('Invalid email!');
        Email::create($value);
    }

    public function invalidEmailDataProvider(): array
    {
        return [
            ['text'],
            ['123'],
            ['user.example.com'],
            ['@user.example.com'],
            ['user.example.com@'],
            ['user@com'],
            ['@user'],
            ['user@']
        ];
    }
}
